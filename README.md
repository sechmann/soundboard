# Soundboard

Simple app to let people play sounds on a shared device.

## Requirements
`pip install -r requirements.txt`
To play audio files other than wav, you need http://avbin.github.io/AVbin/Home/Home.html installed
