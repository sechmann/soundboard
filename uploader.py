from flask import Flask, request, flash, redirect, url_for, json, make_response
from werkzeug.utils import secure_filename
import os
import pyglet

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['wav'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 32 * 1024 * 1024

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'no file part in body', 400
        file = request.files['file']
        if file.filename == '':
            return 'no selected file', 400
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return json.jsonify({'uploaded': filename}), 201

@app.route('/sounds', methods=['GET'])
def sounds():
    sounds = json.jsonify(os.listdir(UPLOAD_FOLDER))
    print(sounds)
    return sounds

@app.route('/play', methods=['POST'])
def play():
    sound = './uploads/' + request.json['sound']
    player = pyglet.media.load(sound, streaming=False)
    player.play()
    return json.jsonify({'submitted': True}), 200

@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Allow-Headers'] = 'content-type'
    return response


if __name__ == '__main__':
    if not os.path.isdir(UPLOAD_FOLDER):
        os.mkdir(UPLOAD_FOLDER)
        
    app.run(host='0.0.0.0', port=8080, debug=True)